
const taxCode = [
{
    id:1,
    name:'Food & Beverage',
    tax:(price)=>{return price * 0.1},
    Refundable:true
},
{
    id:2,
    name:'Tobacco',
    tax:(price)=>{return price * 0.02},
    Refundable:false
},
{
    id:3,
    name:'Entertainment',
    tax:(price)=>{
        return (price-100)*0.01
    },
    Refundable:false
},
]
module.exports = taxCode;