const sequelize1 = require('../config.js');
const Sequelize = require('sequelize');
const order= sequelize1.define('order_item', {
  id:{
    primaryKey:true,
    type:Sequelize.INTEGER,
    autoIncrement:true
  },
  Name: {
      type: Sequelize.STRING,
    },
    Tax_Code: {
      type: Sequelize.INTEGER
    },
    Price: {
      type: Sequelize.INTEGER
    }
  },{
    tableName:'order_item'

  });

  module.exports= order;