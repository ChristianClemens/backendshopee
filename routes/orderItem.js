var express = require('express');
var router = express.Router();
let order = require('../model/order_item.js')
let taxCode = require('../model/tax_code')
/* GET users listing. */
router.get('/getall', function(req, res, next) {

  order.findAll().then(orders => {
    let data = orders.map((order)=>{
     
      let tax=  taxCode.filter(x=> x.id == order.Tax_Code).map((data)=>{
        console.log(data)
        return ({taxName:data.name,tax:data.tax(order.Price),amount:order.Price-data.tax(order.Price),reffund:data.Refundable})
      });
      return Object.assign({itemName:order.Name,Tax_Code:order.Tax_Code,Price:order.Price},tax[0]);
    })
    res.send(data)
  })
});
router.post('/order',function(req, res, next) {
  let {body}=req
  let {itemName,Tax_Code,Price}=body
  order.insertOrUpdate({Name:itemName,Tax_Code:Tax_Code,Price:Price})
  res.send("data")

})
module.exports = router;
